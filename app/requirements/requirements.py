class Requirement:
    OPERATORS = ('eq', 'gte', 'lte', 'gt', 'lt')
    OPERATOR_MAPPING = {
        'eq': '=',
        'gte': '>=',
        'lte': '<=',
        'gt': '>',
        'lt': '<'
    }

    def __init__(self, op):
        if op in self.OPERATOR_MAPPING:
            self.op = self.OPERATOR_MAPPING[op]
        else:
            raise ValueError('Invalid operator')


class ChemistryRequirement(Requirement):
    TYPE = 'chem_function'

    def __init__(self, chemistry, op='gte'):
        self.chemistry = chemistry
        super().__init__(op)


class TeamRatingRequirement(Requirement):
    TYPE = 'metric'

    def __init__(self, team_rating, op='gte'):
        self.team_rating = team_rating
        super().__init__(op)


class CostRequirement(Requirement):
    TYPE = 'metric'

    def __init__(self, costs, op='lte'):
        self.costs = costs
        super().__init__(op)

