# generic mapping
MAIN_PLAYER_TABLE = '#repTb > tbody > tr'


# player mapping
PLAYER_NAME = 'td.table-row-text > a'
PLAYER_ASSET_ID = 'td.table-row-text > img'
PLAYER_FUT_BIN_ID = 'td.table-row-text > a'
PLAYER_POSITION = 'td:nth-of-type(3)'
PLAYER_RATING = 'td:nth-of-type(2) > span'
PLAYER_PS_PRICE = 'td:nth-of-type(5) > span'
PLAYER_XBOX_PRICE = 'td:nth-of-type(6) > span'
PLAYER_PC_PRICE = 'td:nth-of-type(7) > span'
PLAYER_CLUB_ID = 'td.table-row-text > span > a:nth-of-type(1)'
PLAYER_NATION_ID = 'td.table-row-text > span > a:nth-of-type(2)'
PLAYER_LEAGUE_ID = 'td.table-row-text > span > a:nth-of-type(3)'
