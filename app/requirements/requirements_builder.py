from app.requirements.requirements import TeamRatingRequirement, ChemistryRequirement, CostRequirement


class RequirementsBuilder:
    """ Builds a list of requirement objects"""

    def __init__(self, chemistry, team_rating, max_costs, formation):
        self.chemistry = chemistry
        self.team_rating = team_rating
        self.max_costs = max_costs
        self.formation = formation

    def build(self):
        return [ChemistryRequirement(self.chemistry),
                TeamRatingRequirement(self.team_rating),
                CostRequirement(self.max_costs)]
