class RequirementsScorer:
    """ Process"""

    def __init__(self, players_on_positions, requirements):
        self.players_on_positions = players_on_positions
        self.requirements = requirements

    def get_total_score(self):
        for requirement in self.requirements:
            self.score_requirement(requirement)
            pass

    def score_requirement(self, requirement):
        pass