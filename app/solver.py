from app.strategies.strategies import GeneticStrategy


class Solver:
    STRATEGIES = ('genetic', 'knapsack')

    def __init__(self, requirements):
        self.requirements = requirements

    def solve(self, strategy):
        if strategy == 'genetic':
            return GeneticStrategy(self.requirements).run()
        else:
            return None
