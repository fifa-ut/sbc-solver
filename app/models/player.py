# https://www.futbin.com/18/players?page=99&player_rating=79-98&sort=ps_price&order=desc

from mongoengine import IntField, StringField, Document
from app.config import database


class Player(Document):
    asset_id = IntField(unique_with=['rating', 'fut_bin_id'])
    name = StringField()
    position = StringField()
    rating = IntField()
    fut_bin_id = IntField(unique=True)
    ps_price = IntField()
    xbox_price = IntField()
    pc_price = IntField()
    club_id = IntField()
    nation_id = IntField()
    league_id = IntField()

    @classmethod
    def insert_or_update(cls, player):
        Player.objects(fut_bin_id=player.fut_bin_id, asset_id=player.asset_id, rating=player.rating) \
            .modify(upsert=True,
                    new=True,
                    set__name=player.name,
                    set__ps_price=player.ps_price,
                    set__xbox_price=player.xbox_price,
                    set__pc_price=player.pc_price,
                    set__rating=player.rating,
                    set__asset_id=player.asset_id,
                    set__position=player.position,
                    set__nation_id=player.nation_id,
                    set__league_id=player.league_id,
                    set__club_id=player.club_id)
