class BaseStrategy:
    """ Base class to serve strategies, containing the requirements"""
    def __init__(self, requirements):
        self.requirements = requirements


class GeneticStrategy(BaseStrategy):
    """ Uses a genetic algorithm"""

    def __init__(self, requirements):
        super().__init__(requirements)

    def run(self):
        pass


class KnapsackStrategy(BaseStrategy):
    """ Knapsack solver"""

    def __init__(self, requirements):
        super().__init__(requirements)

    def run(self):
        pass

