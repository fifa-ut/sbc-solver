import os
import requests
import app.data_sources.scrapers.futbin_mapping as mapping

from app.config.log_factory import setup_logger
from app.models.player import Player
from bs4 import BeautifulSoup

logger = setup_logger(os.path.basename(__file__), 'scraper.log')  # TODO; fix path


class FutBinScraper:
    BASE_URL = "https://www.futbin.com/18/players"
    SORT_PARAM = "sort=ps_price&order=desc"

    def run(self, rating_min, rating_max, start_page=1, end_page=99):
        logger.info('Started scraping...')
        for page in range(start_page, end_page + 1):
            logger.info(f'Page {page}/{end_page}')
            result, players_processed = self.scrape(page=page, rating_min=rating_min, rating_max=rating_max)
            if not result or players_processed == 0:
                logger.info('Scanned all pages.')
                break

    def scrape(self, page, rating_min, rating_max):

        url = self.get_scrape_url(page, rating_min, rating_max)
        results = requests.get(url)
        soup = BeautifulSoup(results.text, "html.parser")
        return self.process_soup(soup.select(mapping.MAIN_PLAYER_TABLE))

    def process_soup(self, html_table):
        players_processed = 0
        for i, row in enumerate(html_table):
            try:
                row_soup = BeautifulSoup(str(row), "html.parser")
                player = Player()
                try:
                    player.name = row_soup.select(mapping.PLAYER_NAME)[0].text
                    player.fut_bin_id = int(row_soup.select(mapping.PLAYER_FUT_BIN_ID)[0]['href'].split('/')[3])
                    player.asset_id = row_soup.select(mapping.PLAYER_ASSET_ID)[0]['src'].split('/')[-1].split('.')[
                        0].replace('p', '')  # weird ronaldo 'p')
                    player.position = row_soup.select(mapping.PLAYER_POSITION)[0].text
                    player.rating = row_soup.select(mapping.PLAYER_RATING)[0].text
                    player.ps_price = self.price_to_int(row_soup.select(mapping.PLAYER_PS_PRICE)[0].text)
                    player.xbox_price = self.price_to_int(row_soup.select(mapping.PLAYER_XBOX_PRICE)[0].text)
                    player.pc_price = self.price_to_int(row_soup.select(mapping.PLAYER_PC_PRICE)[0].text)
                    player.nation_id = row_soup.select(mapping.PLAYER_NATION_ID)[0]['href'].split('=')[-1]
                    player.league_id = row_soup.select(mapping.PLAYER_LEAGUE_ID)[0]['href'].split('=')[-1]
                    player.club_id = row_soup.select(mapping.PLAYER_CLUB_ID)[0]['href'].split('=')[-1]
                except:
                    logger.warning('Problem processing a row.')
                    continue
                Player.insert_or_update(player)
                players_processed += 1
            except:
                logger.warning(f'Error updating record: {str(player)}')
                raise
        return True, players_processed

    @staticmethod
    def price_to_int(price_string):
        if 'M' in price_string:
            return int(float(price_string[:-1]) * 1000000)
        if 'K' in price_string:
            return int(float(price_string[:-1]) * 1000)

    def get_scrape_url(self, page, rating_min, rating_max):
        return self.BASE_URL + f'?page={page}&player_rating={rating_min}-{rating_max}&' + self.SORT_PARAM
